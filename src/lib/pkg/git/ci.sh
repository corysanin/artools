#!/bin/bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

[[ -z ${ARTOOLS_INCLUDE_GIT_CI_SH:-} ]] || return 0
ARTOOLS_INCLUDE_GIT_CI_SH=1

# shellcheck source=src/lib/pkg/db/db.sh
source "${LIBDIR}"/pkg/db/db.sh

set -e


artixpkg_git_ci_usage() {
    local -r COMMAND=${_ARTOOLS_COMMAND:-${BASH_SOURCE[0]##*/}}
    cat <<- _EOF_
    Usage: ${COMMAND} [OPTIONS] [PKGBASE]...

    OPTIONS
        -a, --agent NAME       Set the CI agent (default: ${AGENTS[0]})
                               Possible values: $(yaml_array ${AGENTS[@]})
        -h, --help             Show this help text

    EXAMPLES
        $ ${COMMAND} --agent ${AGENTS[1]} libfoo
_EOF_
}


artixpkg_git_ci() {
    # options
    local paths=()

    local AGENT=${AGENTS[0]}
    local SWITCH=0
    local CREATED=0

    # variables
    local path realpath pkgbase

    while (( $# )); do
        case $1 in
        -h|--help)
            artixpkg_git_ci_usage
            exit 0
        ;;
        -a|--agent)
            (( $# <= 1 )) && die "missing argument for %s" "$1"
            AGENT="$2"
            SWITCH=1
            shift 2
        ;;
        --)
            shift
            break
        ;;
        -*)
            die "invalid argument: %s" "$1"
        ;;
        *)
            paths=("$@")
            break
        ;;
        esac
    done

    # check if invoked without any path from within a packaging repo
    if (( ${#paths[@]} == 0 )); then
        if [[ -f PKGBUILD ]]; then
            paths=(".")
        else
            artixpkg_git_ci_usage
            exit 1
        fi
    fi

    for path in "${paths[@]}"; do
        if ! realpath=$(realpath -e "${path}"); then
            error "No such directory: ${path}"
            continue
        fi

        pkgbase=$(basename "${realpath}")
        pkgbase=${pkgbase%.git}

        if [[ ! -d "${path}/.git" ]]; then
            error "Not a Git repository: ${path}"
            continue
        fi
        ( cd "${path}" || return

            if [[ ! -f ${REPO_CI} ]]; then

                [[ -d .artixlinux ]] || mkdir .artixlinux

                msg "Adding ci support ..."
                write_jenkinsfile "${AGENT}"

                git add "${REPO_CI}"
                git commit -m "add ci support"

                CREATED=1
            fi

            if [[ ! -f ${REPO_DB} ]]; then

                msg "Creating repo db ..."
                create_repo_db

                if [[ -f PKGBUILD ]]; then
                    # shellcheck source=contrib/makepkg/PKGBUILD.proto
                    source PKGBUILD
                    update_yaml_base
                fi
                git add "${REPO_DB}"
                git commit -m "create repo db"

            fi

            if (( SWITCH )); then
                msg "Switching to agent (${AGENT}) ..."
                write_jenkinsfile "${AGENT}"

                if (( ! CREATED )); then
                    git add "${REPO_CI}"
                    git commit -m "switch agent"
                fi
            fi

        )
    done
}
